import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tqrc_app/routes/routes.dart';
import 'package:tqrc_app/src/services/enums.dart';
import 'package:tqrc_app/src/services/local_storage.dart';
import 'package:tqrc_app/src/utils/app_theme.dart';
import 'package:tqrc_app/src/views/base/helper_methods.dart';
import 'package:tqrc_app/src/views/base/k_logo.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;

  setSplashDuration() async {
    return Timer(
      const Duration(seconds: 3),
          () => pageNavigation(),
    );
  }

  void pageNavigation() async {
    final token = LocalStorage.getData(key: LocalStorageKey.token);
    final tenant = LocalStorage.getData(key: LocalStorageKey.tenant);

    kPrint('Token Value: $token');
    kPrint('Tenant Value: $tenant');

    if(token != null && tenant != null){
      Get.offAllNamed(RouteGenerator.home);
    } else{
      Get.offAllNamed(RouteGenerator.login);
    }
  }

  @override
  void initState() {
    /// hide status bar only for splash screen
    AppThemeData.hideStatusBar();

    /// set splash duration and what next after this duration
    setSplashDuration();

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    animation = CurvedAnimation(
      parent: animationController,
      curve: Curves.fastOutSlowIn,
    );

    animation.addListener(() => setState(() {}));
    animationController.forward();

    super.initState();
  }

  @override
  void dispose() {
    /// since we change app status bar so we need to re-enable default settings
    /// then I set dark status bar for all screen
    AppThemeData.enableInitialThemeSetting();
    AppThemeData.setDarkStatusBar();

    /// dispose animation
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: KLogo(
          height: animation.value * 150,
          width: animation.value * 150,
        ),
      ),
    );
  }
}
