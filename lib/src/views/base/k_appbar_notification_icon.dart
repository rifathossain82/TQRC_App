import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tqrc_app/routes/routes.dart';
import 'package:tqrc_app/src/controllers/notification_controller.dart';
import 'package:tqrc_app/src/utils/color.dart';
import 'package:tqrc_app/src/utils/dimensions.dart';
import 'package:tqrc_app/src/utils/styles.dart';

class KAppBarNotificationIcon extends StatelessWidget {
  const KAppBarNotificationIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.toNamed(RouteGenerator.notifications),
      child: Container(
        padding: EdgeInsets.all(Dimensions.paddingSizeSmall),
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: Obx(() {
          int totalUnreadNotification = Get.find<NotificationController>().totalUnreadNotification.value;
          return totalUnreadNotification < 1
              ? Icon(
                  Icons.notifications,
                  color: mainColor,
                )
              : Stack(
                  children: [
                    Icon(
                      Icons.notifications,
                      color: mainColor,
                    ),
                    Positioned(
                      top: -3,
                      right: -0,
                      child: Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          color: kOrange,
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          '$totalUnreadNotification',
                          style: h6.copyWith(
                            color: kWhite,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                );
        }),
      ),
    );
  }
}
